# This code doesn't work yet!

from __future__ import print_function
from __future__ import division
from mpi4py import MPI
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

import numpy as np
import glob
import matplotlib.pyplot as plt
import scipy.linalg as sl
import argparse

import enterprise
from enterprise.pulsar import Pulsar
import enterprise.signals.parameter as parameter
from enterprise.signals import utils
from enterprise.signals import signal_base
from enterprise.signals import selections
from enterprise.signals.selections import Selection
from enterprise.signals import white_signals
from enterprise.signals import gp_signals
from enterprise.signals import deterministic_signals

from enterprise_extensions import models, model_utils

import scipy.stats as stats
import corner
from PTMCMCSampler.PTMCMCSampler import PTSampler as ptmcmc
import sys


class plotCorner(object):

    def __init__(self, burn, pulsar, saveplot=False):

        self.burn        = burn      # Number of samples to burn
        self.pulsar      = pulsar    # Name of pulsar
        self.saveplot    = saveplot  # Save plot

    @staticmethod
    def check_file(thisfile):
        # TO DO: Implement
        return True

    def get_chains(self):
        chainsfile  = 'chains/' + self.pulsar + '/chain_1.txt'
        self.check_file(chainsfile)
        chains = np.loadtxt(chainsfile)
        return chains

    def get_pars(self):
        parsfile = 'chains/' + self.pulsar + '/pars.txt'
        self.check_file(parsfile)
        pars = np.loadtxt(parsfile, dtype=np.unicode_)
        return pars

    @staticmethod
    def set_namedict(pars):
        namedict = {}
        for par in pars:
            if "GLF" in str(par):
                fields = par.split('_')
                thispar = str(fields[2]) + "_" + str(fields[3])
                namedict[par] = thispar 
            elif "GLTD" in str(par):
                fields = par.split('_')
                thispar = str(fields[2]) + "_" + str(fields[3])
                namedict[par] = thispar
            elif "GLEP" in str(par):
                fields = par.split('_')
                thispar = str(fields[2]) + "_" + str(fields[3])
                namedict[par] = thispar 
            elif "log10_A" in str(par):
                namedict[par] = "TNRedAmp"
            elif "gamma" in str(par):
                namedict[par] = "TNRedGam"
            else:
                fields = par.split('_')
                thispar = (fields[-1]).upper()
                namedict[par] = thispar

        return namedict

    @staticmethod
    def set_transdict(pars):
        transdict = {}
        for par in pars:
            if "F0D" in par:
                thisconv = 1e-6
                transdict[par] = thisconv
            elif "GLF0" in par:
                thisconv = 1e-6
                transdict[par] = thisconv
            elif "GLF1" in par:
                thisconv = 1e-12
                transdict[par] = thisconv
            elif "GLF2" in par:
                thisconv = 1e-20
                transdict[par] = thisconv
            else:
                thisconv = 1.0
                transdict[par] = thisconv

        return transdict
               
    def show_posteriors(self):
        for i in range(0, len(self.pars)):
            multiplier = self.transdict[self.pars[i]]
            thispar = self.namedict[self.pars[i]]
            thismean = np.mean(self.chains[self.burn:,i]) * multiplier
            thissigma = np.std(self.chains[self.burn:,i]) * multiplier
            print(thispar, thismean, thissigma)

    def plot(self):
        ndim = len(self.pars)
        labels = ["" for x in range(ndim)] 
        print(len(labels))
        for i in range(len(self.pars)):
           labels[i] = self.namedict[self.pars[i]]
        corner.corner(self.chains[self.burn:,:len(self.pars)], labels=labels, smooth=True)        
        plt.savefig("corner.pdf", format='pdf', dpi=400)
        plt.close()

    def run(self):

        if rank == 0:

            # Check for and load chains
            self.chains = self.get_chains()

            # Check for and load parameters
            self.pars = self.get_pars()

            # Set dictionary of axis labels for corner
            self.namedict = self.set_namedict(self.pars)

            # Set dictionary of conversion factors for parameters
            self.transdict = self.set_transdict(self.pars)

            # Show posteriors
            self.show_posteriors()

            # Make corner plot
            self.plot()

def main():

    parser = argparse.ArgumentParser(description='Make corner plot from MCMC chains')
    parser.add_argument('-p','--pulsar', help='Name of pulsar (Default=B0531+21)', required=False, type=str, default='B0531+21')
    parser.add_argument('-b','--burn', help='Number of samples to burn', required=True, type=int)
    parser.add_argument('-s','--saveplot', help='Save plot as pdf', required=False, action='store_true')
    args = parser.parse_args()

    plotter = plotCorner(args.burn, args.pulsar, args.saveplot)
    plotter.run()


if __name__ == '__main__':
    main()

